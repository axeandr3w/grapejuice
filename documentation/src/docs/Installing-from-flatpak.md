title: Install from flatpak
---
**Table of contents**

[TOC]

## Prerequisites

Please ensure that before following this guide you complete the [flatpak setup](https://flatpak.org/setup/) for your respective distro. Please also ensure that you have [wine](Installing-Wine) installed.

---
## Installation guide
The flatpak version of Grapejuice can be found [here](https://flathub.org/apps/details/net.brinkervii.grapejuice)

In order to install the needed packages you will need to first run`flatpak install flathub net.brinkervii.grapejuice`

To start Grapejuice run the command `flatpak run net.brinkervii.grapejuice`

---

## Troubleshooting

If you are having trouble running Grapejuice try searching for it in your distro's application manager or you can check the [troubleshooting page](Troubleshooting), you could also ask for help in the [community discord server](https://discord.gg/mRTzEb6).
